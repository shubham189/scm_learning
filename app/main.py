from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}
    
@app.get("/test")
def read_test():
    return {"testing": "Successful"}
    

@app.get("/add")
def read_test():
    return {"put_number": "click here to put number"}

@app.get("/sub")
def sub_test():
    return {"put_number": "click here to put number"}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}